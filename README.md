# ndaal_public_Ansible_Roles_Hardening_Debian_11



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ndaal_open_source/ndaal_public_ansible_roles_hardening_debian_11.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ndaal_open_source/ndaal_public_ansible_roles_hardening_debian_11/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


## Scorecard

Aggregate score: 9.5 / 10

Check scores:


| SCORE   | NAME             | REASON                                                          | DETAILS | DOCUMENTATION/REMEDIATION                                                                                         |
|---------|------------------|-----------------------------------------------------------------|---------|-------------------------------------------------------------------------------------------------------------------|
| 10 / 10 | Binary-Artifacts | no binaries found in the repo                                   |         | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#binary-artifacts> |
| 8 / 10  | Code-Review      | found 1 unreviewed changesets out of 5 -- score normalized to 8 |         | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#code-review>      |
| |   | 8 |   | / |   | 1 | 0 |   | | |   | C | o | d | e | - | R | e | v | i | e | w |   | | |   | f | o | u | n | d |   | 1 |   | u | n | r | e | v | i | e | w | e | d |   | c | h | a | n | g | e | s | e | t | s |   | o | u | t |   | o | f |   | 5 |   | - | - |   | s | c | o | r | e |   | n | o | r | m | a | l | i | z | e | d |   | t | o |   | 8 |   | | |   |   | | |   | h | t | t | p | s | : | / | / | g | i | t | h | u | b | . | c | o | m | / | o | s | s | f | / | s | c | o | r | e | c | a | r | d | / | b | l | o | b | / | 4 | 9 | c | 0 | e | e | d | 3 | a | 4 | 2 | 3 | f | 0 | 0 | c | 8 | 7 | 2 | b | 5 | c | 3 | c | 9 | f | 1 | b | b | c | a | 9 | e | 8 | a | a | e | 7 | 9 | 9 | / | d | o | c | s | / | c | h | e | c | k | s | . | m | d | # | c | o | d | e | - | r | e | v | i | e | w |   | |
| |   | | |   |   |   | | |   | 8 |   | | |   |   |   | | |   | / |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | C |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | R |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | f |   | | |   | o |   | | |   | u |   | | |   | n |   | | |   | d |   | | |   |   |   | | |   | 1 |   | | |   |   |   | | |   | u |   | | |   | n |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | c |   | | |   | h |   | | |   | a |   | | |   | n |   | | |   | g |   | | |   | e |   | | |   | s |   | | |   | e |   | | |   | t |   | | |   | s |   | | |   |   |   | | |   | o |   | | |   | u |   | | |   | t |   | | |   |   |   | | |   | o |   | | |   | f |   | | |   |   |   | | |   | 5 |   | | |   |   |   | | |   | - |   | | |   | - |   | | |   |   |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   |   |   | | |   | n |   | | |   | o |   | | |   | r |   | | |   | m |   | | |   | a |   | | |   | l |   | | |   | i |   | | |   | z |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | t |   | | |   | o |   | | |   |   |   | | |   | 8 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | h |   | | |   | t |   | | |   | t |   | | |   | p |   | | |   | s |   | | |   | : |   | | |   | / |   | | |   | / |   | | |   | g |   | | |   | i |   | | |   | t |   | | |   | h |   | | |   | u |   | | |   | b |   | | |   | . |   | | |   | c |   | | |   | o |   | | |   | m |   | | |   | / |   | | |   | o |   | | |   | s |   | | |   | s |   | | |   | f |   | | |   | / |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | a |   | | |   | r |   | | |   | d |   | | |   | / |   | | |   | b |   | | |   | l |   | | |   | o |   | | |   | b |   | | |   | / |   | | |   | 4 |   | | |   | 9 |   | | |   | c |   | | |   | 0 |   | | |   | e |   | | |   | e |   | | |   | d |   | | |   | 3 |   | | |   | a |   | | |   | 4 |   | | |   | 2 |   | | |   | 3 |   | | |   | f |   | | |   | 0 |   | | |   | 0 |   | | |   | c |   | | |   | 8 |   | | |   | 7 |   | | |   | 2 |   | | |   | b |   | | |   | 5 |   | | |   | c |   | | |   | 3 |   | | |   | c |   | | |   | 9 |   | | |   | f |   | | |   | 1 |   | | |   | b |   | | |   | b |   | | |   | c |   | | |   | a |   | | |   | 9 |   | | |   | e |   | | |   | 8 |   | | |   | a |   | | |   | a |   | | |   | e |   | | |   | 7 |   | | |   | 9 |   | | |   | 9 |   | | |   | / |   | | |   | d |   | | |   | o |   | | |   | c |   | | |   | s |   | | |   | / |   | | |   | c |   | | |   | h |   | | |   | e |   | | |   | c |   | | |   | k |   | | |   | s |   | | |   | . |   | | |   | m |   | | |   | d |   | | |   | # |   | | |   | c |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | |
| 10 / 10 | License | license file detected | Info: License file found in expected location: LICENSE:1 Info: FSF or OSI recognized license: LICENSE:1 | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#license> |
| |   | 1 | 0 |   | / |   | 1 | 0 |   | | |   | L | i | c | e | n | s | e |   | | |   | l | i | c | e | n | s | e |   | f | i | l | e |   | d | e | t | e | c | t | e | d |   | | |   | I | n | f | o | : |   | L | i | c | e | n | s | e |   | f | i | l | e |   | f | o | u | n | d |   | i | n |   | e | x | p | e | c | t | e | d |   | l | o | c | a | t | i | o | n | : |   | L | I | C | E | N | S | E | : | 1 |   | I | n | f | o | : |   | F | S | F |   | o | r |   | O | S | I |   | r | e | c | o | g | n | i | z | e | d |   | l | i | c | e | n | s | e | : |   | L | I | C | E | N | S | E | : | 1 |   | | |   | h | t | t | p | s | : | / | / | g | i | t | h | u | b | . | c | o | m | / | o | s | s | f | / | s | c | o | r | e | c | a | r | d | / | b | l | o | b | / | 4 | 9 | c | 0 | e | e | d | 3 | a | 4 | 2 | 3 | f | 0 | 0 | c | 8 | 7 | 2 | b | 5 | c | 3 | c | 9 | f | 1 | b | b | c | a | 9 | e | 8 | a | a | e | 7 | 9 | 9 | / | d | o | c | s | / | c | h | e | c | k | s | . | m | d | # | l | i | c | e | n | s | e |   | |
| |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | / |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | L |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | f |   | | |   | i |   | | |   | l |   | | |   | e |   | | |   |   |   | | |   | d |   | | |   | e |   | | |   | t |   | | |   | e |   | | |   | c |   | | |   | t |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | I |   | | |   | n |   | | |   | f |   | | |   | o |   | | |   | : |   | | |   |   |   | | |   | L |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | f |   | | |   | i |   | | |   | l |   | | |   | e |   | | |   |   |   | | |   | f |   | | |   | o |   | | |   | u |   | | |   | n |   | | |   | d |   | | |   |   |   | | |   | i |   | | |   | n |   | | |   |   |   | | |   | e |   | | |   | x |   | | |   | p |   | | |   | e |   | | |   | c |   | | |   | t |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | l |   | | |   | o |   | | |   | c |   | | |   | a |   | | |   | t |   | | |   | i |   | | |   | o |   | | |   | n |   | | |   | : |   | | |   |   |   | | |   | L |   | | |   | I |   | | |   | C |   | | |   | E |   | | |   | N |   | | |   | S |   | | |   | E |   | | |   | : |   | | |   | 1 |   | | |   |   |   | | |   | I |   | | |   | n |   | | |   | f |   | | |   | o |   | | |   | : |   | | |   |   |   | | |   | F |   | | |   | S |   | | |   | F |   | | |   |   |   | | |   | o |   | | |   | r |   | | |   |   |   | | |   | O |   | | |   | S |   | | |   | I |   | | |   |   |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | o |   | | |   | g |   | | |   | n |   | | |   | i |   | | |   | z |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   | : |   | | |   |   |   | | |   | L |   | | |   | I |   | | |   | C |   | | |   | E |   | | |   | N |   | | |   | S |   | | |   | E |   | | |   | : |   | | |   | 1 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | h |   | | |   | t |   | | |   | t |   | | |   | p |   | | |   | s |   | | |   | : |   | | |   | / |   | | |   | / |   | | |   | g |   | | |   | i |   | | |   | t |   | | |   | h |   | | |   | u |   | | |   | b |   | | |   | . |   | | |   | c |   | | |   | o |   | | |   | m |   | | |   | / |   | | |   | o |   | | |   | s |   | | |   | s |   | | |   | f |   | | |   | / |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | a |   | | |   | r |   | | |   | d |   | | |   | / |   | | |   | b |   | | |   | l |   | | |   | o |   | | |   | b |   | | |   | / |   | | |   | 4 |   | | |   | 9 |   | | |   | c |   | | |   | 0 |   | | |   | e |   | | |   | e |   | | |   | d |   | | |   | 3 |   | | |   | a |   | | |   | 4 |   | | |   | 2 |   | | |   | 3 |   | | |   | f |   | | |   | 0 |   | | |   | 0 |   | | |   | c |   | | |   | 8 |   | | |   | 7 |   | | |   | 2 |   | | |   | b |   | | |   | 5 |   | | |   | c |   | | |   | 3 |   | | |   | c |   | | |   | 9 |   | | |   | f |   | | |   | 1 |   | | |   | b |   | | |   | b |   | | |   | c |   | | |   | a |   | | |   | 9 |   | | |   | e |   | | |   | 8 |   | | |   | a |   | | |   | a |   | | |   | e |   | | |   | 7 |   | | |   | 9 |   | | |   | 9 |   | | |   | / |   | | |   | d |   | | |   | o |   | | |   | c |   | | |   | s |   | | |   | / |   | | |   | c |   | | |   | h |   | | |   | e |   | | |   | c |   | | |   | k |   | | |   | s |   | | |   | . |   | | |   | m |   | | |   | d |   | | |   | # |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | |
| 10 / 10 | Security-Policy | security policy file detected | Info: security policy file detected: SECURITY.md:1 Info: Found linked content: SECURITY.md:1 Info: Found disclosure, vulnerability, and/or timelines in security policy: SECURITY.md:1 Info: Found text in security policy: SECURITY.md:1 | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#security-policy> |
| 10 / 10 | Vulnerabilities | no vulnerabilities detected |  | <https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#vulnerabilities> |

